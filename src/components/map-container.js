import React from 'react'
import { Map, TileLayer, CircleMarker, Polyline } from 'react-leaflet'

const position = [ 50.8960532, 14.2592132 ]
const zoom = 16

function MapContainer({ waypoints, addWaypoint }) {
  return (
    <section className="map">
      <Map
        onclick={addWaypoint}
        center={position} 
        zoom={zoom}>
        <TileLayer
          attribution="&copy; Komoot"
          url="https://{s}.tile.hosted.thunderforest.com/komoot-2/{z}/{x}/{y}.png"
        />
        {waypoints.map((waypoint, i) => 
          <CircleMarker 
            color={i === 0 ? 'red': 'blue'} 
            center={waypoint.latlng} 
            key={i} />
        )}
        <Polyline 
          color="lime"
          positions={waypoints.map(waypoint => waypoint.latlng)}
        />
      </Map>
    </section>
  )
 
}

export default MapContainer;