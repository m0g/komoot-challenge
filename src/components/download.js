import React from 'react'

function Download({ waypoints }) {
  const generateGPX = () => {
    const xml = `
      <?xml version='1.0' encoding='UTF-8'?>
      <gpx version="1.1" creator="https://www.komoot.de" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
        <metadata>
          <name>Schönefeld Highway – Waßmannsdorfer Chaussee Loop from Leinestraße</name>
          <author>
            <link href="https://www.komoot.de">
              <text>komoot</text>
              <type>text/html</type>
            </link>
          </author>
        </metadata>
        <trk>
          <name>Komoot Challenge</name>
          <trkseg>
            ${waypoints.map(w => `
              <trkpt lat="${w.latlng.lat}" lon="${w.latlng.lng}">
                <ele>0</ele>
              </trkpt>
            `).join('')}
          </trkseg>
        </trk>
      </gpx>
    `

    const filename = "route.gpx";
    const file = new Blob([xml.trim()], { type: 'application/gpx+xml' });
    const link = document.createElement('a');

    link.href = window.URL.createObjectURL(file);
    link.download = filename;

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
 
  };

  return (
    <button 
      className="download" 
      onClick={generateGPX}>Download GPX</button>
  )
}

export default Download;