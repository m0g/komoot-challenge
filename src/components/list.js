import React from 'react'
import Waypoint from './waypoint';
import Download from './download';

function indexInParent(node) {
  const children = node.parentNode.childNodes;
  let num = 0;

  for (let i = 0; i < children.length; i++) {
    if (children[i] === node) {
      return num;
    }

    if (children[i].nodeType === 1) {
      num++;
    }
  }

  return -1;
}

class List extends  React.Component {
  state = {
    drag: {
      origin: 0
    }
  }

  // End
  handleDragEnd = (e, index) => {
    let state = this.state

    state.drag.origin = index
    this.setState(state)
    this.props.moveWaypoint(state.drag)
  }

  onDragOver = e => {
    let target = e.target
    let state = this.state

    if (target.nodeName !== 'LI') {
      target = target.parentNode
    }

    state.drag.target = indexInParent(target)
    this.setState(state)
  }

  render() {
    const { waypoints, removeWaypoint } = this.props

    return (
      <section className="list">
        <h2>Route Builder</h2>        
        <ul
          onDragOver={this.onDragOver}
        >
          {waypoints.map((waypoint, i) => 
            <Waypoint 
              onDragEnd={this.handleDragEnd}
              removeWaypoint={removeWaypoint}
              index={i}
              key={i} />)}
        </ul>
        {waypoints.length > 0 && <Download waypoints={waypoints} />}
      </section>
    )
  }
}

export default List;