import React from 'react'

function Waypoint({ removeWaypoint, index, onDragEnd }) {
  return (
    <li 
      style={{background: index === 0 ? 'red' : 'transparent'}}
      onDragStart={e => e.dataTransfer.setData('text','')}
      onDragEnd={e => onDragEnd(e, index)}
      draggable>
      <span className="draggable">&#x22ee;</span>
      <span>Waypoint {index + 1}</span>
      <button onClick={() => removeWaypoint(index)}>🗑️</button>
    </li>
  )
}

export default Waypoint;