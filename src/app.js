import React, { Component } from 'react'
import MapContainer from './components/map-container'
import List from './components/list'
import Leaflet from 'leaflet'

Leaflet.Icon.Default.imagePath =
'//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.4/images/'

class App extends Component {
  state = {
    waypoints: []
  }

  removeWaypoint = i => {
    let state = this.state

    state.waypoints = state.waypoints
      .filter((w, index) => index !== i)

    this.setState(state)
  }

  addWaypoint = e => {
    let state = this.state

    state.waypoints.push({ 
      latlng: e.latlng 
    });

    this.setState(state)
  }

  moveWaypoint = ({ origin, target }) => {
    let state = this.state
    let [waypoint] = state.waypoints.splice(origin, 1)

    state.waypoints.splice(target, 0, waypoint)
    this.setState(state)
  }

  render() {
    return (
      <div className="app">
        <List 
          waypoints={this.state.waypoints}
          removeWaypoint={this.removeWaypoint}
          moveWaypoint={this.moveWaypoint}
        />
        <MapContainer 
          addWaypoint={this.addWaypoint} 
          waypoints={this.state.waypoints}
        />
      </div>
    );
  }
}

export default App;
